const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];


// 21 cell 123; 15 row ABC;


// Função para criar linhas
function newRow() {
    for (let count = 0; count <= 15; count++) {
        let x = document.createElement("div");
        x.id = "row" + count;
        document.body.appendChild(x);
    }
}
newRow();


// Função para criar celulas -------------
function newCell() {
    for (countLinha = 0; countLinha < 15; countLinha++) {
        let rowSelect = document.getElementById("row" + countLinha);
        for (let countColuna = 0; countColuna < 21; countColuna++) {
            let x = document.createElement("div");
            x.id = "cell" + countColuna;
            rowSelect.appendChild(x);

            if (map[countLinha][countColuna] == "W") {
                x.classList.add("wall");
            }
            // x.classList.add("cell");
            // console.log(n);
        }

    }
}
newCell();

// Adicionar ponto de partida -------------
let start = document.createElement("div");
let startCellSelector = document.querySelector("#row9 #cell0");
start.id = "start";
startCellSelector.appendChild(start);


// Contadores Iniciados
countCell = 0;
countRow = 9;

let win = document.createElement("h1");
win.textContent = "Obrigado por jogar o lixo no lixo!";


// Eventos capturados, soma ou diminui contador -------------
document.addEventListener('keydown', (event) => {

    const keyName = event.key;

    if (keyName === "ArrowUp") {
        countRow--;
        if (map[countRow][countCell] == "W") {
            loseSound();
            reloadWeb();
        }
        if (countRow < 0) {
            loseSound();
            reloadWeb();
        }
        rightSound();
        document.getElementById("start").style.animationName = "slideTop";
        document.querySelector("#row" + countRow + " #cell" + countCell).appendChild(start);
        removeAnimation();
    }

    if (keyName === "ArrowRight") {
        countCell++;

        // Condição de vitória

        if (countCell == 20 && countRow == 8) {
            winnerSound();
            document.querySelector("#row9 #cell0").appendChild(start);
            countCell = 0;
            countRow = 9;   
            document.body.appendChild(win);
            return
        }

        if (map[countRow][countCell] == "W") {
            loseSound();
            reloadWeb();
        }
        if (countCell > 20) {
            loseSound();
            reloadWeb();
        }
        rightSound();
        document.getElementById("start").style.animationName = "slideRight";
        document.querySelector("#row" + countRow + " #cell" + countCell).appendChild(start);
        removeAnimation();

    }

    if (keyName === "ArrowDown") {
        countRow++;
        if (map[countRow][countCell] == "W") {
            loseSound();
            reloadWeb();
        }
        if (countRow > 15) {
            loseSound();
            reloadWeb();
        }
        rightSound();
        document.getElementById("start").style.animationName = "slideDown";
        document.querySelector("#row" + countRow + " #cell" + countCell).appendChild(start);
        removeAnimation();
    }

    if (keyName === "ArrowLeft") {
        countCell--;
        if (map[countRow][countCell] == "W") {
            loseSound();
            reloadWeb();
        }
        if (countCell < 0) {
            loseSound();
            reloadWeb();
        }
        rightSound();
        document.getElementById("start").style.animationName = "slideLeft";
        document.querySelector("#row" + countRow + " #cell" + countCell).appendChild(start);
        removeAnimation();

    }
});

// Funções para animação

function removeAnimation() {
    setTimeout(function() { document.getElementById("start").style.animationName = "none";}, 110);
}

function rightSound() {
    let sound = new Audio("./correct.mp3")
    sound.play();
}

function loseSound() {
    let sound = new Audio("./perdeu.mp3")
    sound.volume = 0.5;
    sound.play();
}

function winnerSound() {
    let sound = new Audio("./winner.mp3")
    sound.play();
}

function reloadWeb() {
    setTimeout(function() {document.location.reload(true);}, 650)
}